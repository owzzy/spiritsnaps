using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Renderer>().isVisible)
        {
            if(Input.GetKey("space")) // Ideally this would be the trigger on the controller but I couldn't find
                                       // the right input command for VR controllers without using SteamVR input.
            {
                ScreenshotHandler.TakeScreenshot_Static(Screen.width, Screen.height); // Take a screenshot
            }
            else
            {
                // do nothing
            }
            //gameObject.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
            Debug.Log("Ghost Visible");
        }
        else
        {
            //gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 0);
            Debug.Log("Ghost Not Visible");
        }
    }
}
